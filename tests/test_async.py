from gtkme import async
import pytest
import threading
import time


def test_basic():
    future = async.Future()
    assert not future.is_ready()

    def do_result():
        future.result('ok')
    thread = async.spawn_thread(do_result)
    assert future.wait() == 'ok'
    assert future.is_ready()
    thread.join()


def test_exception():

    def do_exception():
        raise Exception("It broke!")

    future = async.Future()
    future.run(do_exception)

    with pytest.raises(Exception):
        future.wait()


@pytest.mark.parametrize('blocking,delay,count',
                         [(True, 0.5, 4),
                          (False, 2, 1000)])
def test_holding(blocking, delay, count):

    lock = threading.Lock()
    shared_var = [0]

    def do_thread():
        assert shared_var[0] == 0
        shared_var[0] += 1
        time.sleep(delay)
        assert shared_var[0] == 1
        shared_var[0] -= 1

    results = []
    for i in range(count):
        results.append(async.holding(lock, do_thread, blocking))

    for r in results:
        if r is not None:
            r.wait()


def test_debounce():
    """Put DebounceSyncVar through its paces

    We create a dsv and with a delay of 1 second and launch two threads
    in parallel. One thread collects values from the dsv repeatedly.
    The other submits ten values, the first five with replace(), the rest
    with put().

    Unless the machine this is running on is very slow, the final result
    should be the last value that was inserted via replace(), followed by
    all of the values inserted with put(); The one-second delay guarantees
    that the first four values will be overwritten.

    This should take about 6 seconds to run.
    """
    dsv = async.DebouncedSyncVar(1)

    def do_replace_put():
        for i in range(0, 5):
            dsv.replace(i)
        for i in range(5, 10):
            dsv.put(i)

    future = async.Future()

    def do_get():
        result = []
        i = 0
        while i < 9:
            i, _ok = dsv.get()
            result.append(i)
        future.result(result)

    async.spawn_thread(do_replace_put)
    async.spawn_thread(do_get)
    result = future.wait()
    assert result == list(range(4, 10))
