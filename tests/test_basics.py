#!/usr/bin/python
#
# Copyright (C) 2013 Martin Owens
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
""""""

import os
import sys
import unittest

#sys.path.insert(0, '../lib/')

#from autopilot import AutopilotTestCase, GtkIntrospectionTestMixin
from autopilot.introspection.gtk import GtkIntrospectionTestMixin

try:
    from test import test_support
except ImportError:
    from test import support as test_support

class GtkTestCase(AutopilotTestCase, GtkIntrospectionTestMixin):
    def setUp(self):
        AutopilotTestCase.setUp(self)
        GtkIntrospectionTestMixin.setUp(self)
        self.application = self.launch_test_application('myappname')

    def test_00_enabled(self):
        """Test Compatability Mode"""
        self.assertTrue(self.application)

if __name__ == '__main__':
    test_support.run_unittest(
       GtkTestCase,
    )
